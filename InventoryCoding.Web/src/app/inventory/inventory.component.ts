import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { KernalData } from '../models/kernalData';
import { InventoryService } from '../service/inventory.service';

@Component({
  selector: 'app-inventory-component',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})

export class InventoryComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'kernels'];
  displayedColumnsKernels: string[] = ['id', 'inventoryId', 'requestedKernels'];
  dataSource: any;
  message: string = "";
  isSuccess: boolean = false;
  inventoryType: string = "";
  public inputKernels = new FormControl;

  constructor(private inventoryService: InventoryService) { }

  ngOnInit(): void {
    this.LoadInventory();
  }

  LoadInventory() {
    this.inventoryService.GetInventory().subscribe((resp) => {
      this.dataSource = new MatTableDataSource<KernalData>(resp);
    });
  }

  CheckKernals(inventoryType: string, id: string) {
    if (id && inventoryType) {
      this.inventoryService.GetKernals(parseInt(inventoryType), parseInt(id))
        .subscribe((resp) => {
          if (resp) {
            this.message = "Requested kernels are available.";
            this.isSuccess = true;
          } else {
            this.message = "Requested kernels are not not available.";
            this.isSuccess = false;
          }
        });
    } else {
      this.message = "Oops! invalid request number or inventor type.";
      this.isSuccess = false;
    }
  }
}
