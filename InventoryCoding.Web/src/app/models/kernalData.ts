import { Inventory } from "./inventory";
export interface KernalData {
    inventory: Inventory[];
    requests: Request[];
}