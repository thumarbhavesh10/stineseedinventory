export interface Request {
    id: number;
    inventoryId: string;
    requestedKernels: number
}