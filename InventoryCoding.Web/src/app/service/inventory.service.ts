import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})

export class InventoryService {
  constructor(private httpClient: HttpClient) { }

  public GetInventory(): Observable<any> {
    const apiUrl = `${environment.apiUrl}/home/inventory`;
    
    return this.httpClient.get<any>(apiUrl).pipe();
  }

  public GetKernals(inventoryType: number, value: number): Observable<any> {
    const apiUrl = `${environment.apiUrl}/home/kernals?inventoryId=${inventoryType}&kernelCount=${value}`;

    return this.httpClient.get<any>(apiUrl).pipe();
  }
}
