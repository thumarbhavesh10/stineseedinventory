﻿using Inventory.Core.Configuration;
using Inventory.Core.Entities;
using Inventory.Core.Interface;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Infrastructure.Service
{
    public class InventoryService : IInventoryService
    {
        private readonly APIConfiguration _aPIConfiguration;
        public InventoryService(IOptions<APIConfiguration> aPIConfiguration)
        {
            _aPIConfiguration = aPIConfiguration.Value;
        }
        public async Task<KernalData> GetInventory()
        {

            RestClient client = new RestClient(_aPIConfiguration.ServiceEndPoint);
            var request = new RestRequest();
            request.Method = Method.Get;
            RestResponse restResponse = await client.ExecuteAsync<string>(request);

            return JsonConvert.DeserializeObject<KernalData>(restResponse.Content);
        }

        public async Task<bool> GetKernals(int inventoryId, int kernelCount)
        {
            RestClient client = new RestClient(_aPIConfiguration.ServiceEndPoint);
            var request = new RestRequest();
            RestResponse restResponse = await client.ExecuteAsync<string>(request);

            var jsonData = JsonConvert.DeserializeObject<KernalData>(restResponse.Content);

            var inventoryCount = jsonData.inventory.FirstOrDefault(item => item.id == inventoryId)?.kernels;
            if (inventoryCount == null || inventoryCount == 0)
                return false;

            var requestCount = jsonData.requests.Where(item => item.inventoryId == inventoryId.ToString())
                .GroupBy(x => x.inventoryId)
                .Select(x => x.Sum(y => Convert.ToInt32(y.requestedKernels))).Sum();

            var remainingInventory = (inventoryCount - requestCount) - kernelCount;
            if (remainingInventory >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
