﻿using Inventory.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Inventory.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly IInventoryService _inventoryService;
        public HomeController(IInventoryService inventoryRepository)
        {
            _inventoryService = inventoryRepository;
        }

        [HttpGet]
        [Route("inventory")]
        public async Task<ActionResult> GetInventory()
        {
            var result = await _inventoryService.GetInventory();
            return Ok(result);
        }

        [HttpGet]
        [Route("kernals")]
        public async Task<ActionResult> GetKernals(int inventoryId, int kernelCount)
        {
            var result = await _inventoryService.GetKernals(inventoryId, kernelCount);
            return Ok(result);
        }
    }
}
