﻿namespace Inventory.Core.Entities
{
    public class Request
    {
        public int id { get; set; }
        public string inventoryId { get; set; }
        public int requestedKernels { get; set; }
    }
}
