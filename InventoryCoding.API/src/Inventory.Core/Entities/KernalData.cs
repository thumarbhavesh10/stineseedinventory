﻿using System.Collections.Generic;

namespace Inventory.Core.Entities
{
    public class KernalData
    {
        public List<Inventory> inventory { get; set; }
        public List<Request> requests { get; set; }
    }
}
