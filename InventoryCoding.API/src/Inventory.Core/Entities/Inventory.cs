﻿
namespace Inventory.Core.Entities
{
    public class Inventory
    {
        public int id { get; set; }
        public string name { get; set; }
        public int kernels { get; set; }
    }
}
