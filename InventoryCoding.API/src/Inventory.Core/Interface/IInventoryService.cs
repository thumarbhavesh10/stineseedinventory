﻿using Inventory.Core.Entities;
using System.Threading.Tasks;

namespace Inventory.Core.Interface
{
    public interface IInventoryService
    {
       Task<KernalData> GetInventory();
       Task<bool> GetKernals(int inventoryId, int kernelCount);
    }
}
